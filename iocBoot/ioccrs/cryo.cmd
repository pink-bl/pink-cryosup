#!../../bin/linux-x86_64/crs

## You may have to change crs to something else
## everywhere it appears in this file

< envPaths

#epicsEnvSet("MOTORX", "PINK:MSIM2:m1")
#epicsEnvSet("MOTORXRBV", "PINK:MSIM2:m1.RBV")
#epicsEnvSet("MOTORY", "PINK:MSIM2:m2")
#epicsEnvSet("MOTORYRBV", "PINK:MSIM2:m2.RBV")

epicsEnvSet("MOTORX", "PINK:PHY:AxisJ.VAL")
epicsEnvSet("MOTORXRBV", "PINK:PHY:AxisJ.RBV")
epicsEnvSet("MOTORY", "PINK:ANC01:ACT0:CMD:TARGET")
epicsEnvSet("MOTORYRBV", "PINK:ANC01:ACT0:POSITION")


cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/crs.dbd"
crs_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/crs.db","BL=PINK, DEV=CRYO, MX=$(MOTORX), MXRBV=$(MOTORXRBV), MY=$(MOTORY), MYRBV=$(MOTORYRBV)")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=CRYO")

## Start any sequence programs
#seq sncxxx,"user=epics"
